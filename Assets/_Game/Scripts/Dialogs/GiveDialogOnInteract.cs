// written by Sebastian Wölffel

using UnityAtoms;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Scripts.Dialogs {
	public class GiveDialogOnInteract : MonoBehaviour {
		[SerializeField] private DialogGiver dialogGiver;
		private bool canInteract;
		private InputActionsAsset inputActions;
		[SerializeField] private BoolVariable isInDialog;

		private void OnEnable() {
			inputActions = new InputActionsAsset();
			inputActions.Gameplay.Interact.performed += StartDialog;
			inputActions.Gameplay.Interact.Enable();
		}

		private void OnDisable() {
			inputActions.Gameplay.Interact.performed -= StartDialog;
			inputActions.Gameplay.Interact.Disable();
		}

		private void OnTriggerEnter(Collider other) {
			if (other.CompareTag("Player")) canInteract = true;
			}

		private void OnTriggerExit(Collider other) {
			if (other.CompareTag("Player")) canInteract = false;
		}

		private void StartDialog(InputAction.CallbackContext context) {
			if (!canInteract) return;
			dialogGiver.StartConversation();
		}
	}
}
