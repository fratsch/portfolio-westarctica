using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Dialogs {
	/// <summary>
	/// Holds several dialogs that should be unlocked upon a certain trigger (could be completion of a previous dialog)
	/// </summary>
	[CreateAssetMenu(fileName = "New DialogUnlocker", menuName = "West Arctica/Dialogs/DialogUnlocker", order = 2)]
	public class DialogUnlocker : ScriptableObject{
		[Tooltip("The Dialogs that should be unlocked if this DialogUnlocker is activated.")]
		[SerializeField] private List<Dialog> dialogsToUnlock = new List<Dialog>();

		// unlocks all Dialogs
		public void UnlockDialogs() {
			foreach (Dialog dialog in dialogsToUnlock) {
				dialog.Unlocked = true;
			}
		}
	}
}
