// written by Sebastian Wölffel

using System.Collections.Generic;
using TMPro;
using UnityAtoms;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Void = UnityAtoms.Void;

namespace Scripts.Dialogs.UI {
	public class DialogCanvasController : MonoBehaviour, InputActionsAsset.IUIActions {
		#region Event Listener Structs

		private struct DialogUpdateEventListener : IGameEventListener<Void> {
			public DialogCanvasController controller;

			public void OnEventRaised(Void item) {
				controller.ShowCurrentDialog();
			}
		}

		private struct StartConversationEventListener : IGameEventListener<Void> {
			public DialogCanvasController controller;

			public void OnEventRaised(Void item) {
				controller.ShowDialogOptions();
				controller.ShowDialog();
			}
		}

		private struct EndConversationEventListener : IGameEventListener<Void> {
			public DialogCanvasController controller;

			public void OnEventRaised(Void item) {
				controller.HideDialog();
			}
		}

		private struct FinishedDialogEventListener : IGameEventListener<Void> {
			public DialogCanvasController controller;

			public void OnEventRaised(Void item) {
				controller.ShowDialogOptions();
			}
		}

		#endregion

		#region Fields and Properties

		//Sounds
		[SerializeField]
		private GameObject dialogeSoundManager;
		private Scr_MenuSounds uiSounds;

		[Header("Dialog Events")]
		[Tooltip("VoidEvent that is raised when the Dialog changes state.")]
		[SerializeField]
		private VoidEvent dialogUpdateEvent;

		[Tooltip("VoidEvent that is raised when after a new DialogSet was sent to the DialogManager.")]
		[SerializeField]
		private VoidEvent startConversationEvent;

		[Tooltip("VoidEvent that is raised when the Dialog changes state.")]
		[SerializeField]
		private VoidEvent endConversationEvent;

		[Tooltip("VoidEvent that is raised when the Dialog is finished.")]
		[SerializeField]
		private VoidEvent finishDialogEvent;

		[Header("Dialog Box Elements")]
		[SerializeField] private GameObject dialogManagerGameObject;
		[SerializeField] private GameObject dialogPanel, dialogTextPanel, dialogIconsPanel, dialogOptionPanel;
		[SerializeField] private GameObject npcIcon, pcIcon;
		[SerializeField] private TextMeshProUGUI dialogText, npcName, pcName;
		[SerializeField] private Image npcImage, npcBackgroundImage, pcImage, pcBackgroundImage, dialogBackground;
		[SerializeField] private Button[] buttons = new Button[3];
		[SerializeField] private Sprite defaultSprite, selectedSprite;
		[SerializeField] private Sprite pcBackgroundSpriteDefault, pcBackgroundSpriteTalking, npcBackgroundSpriteDefault, npcBackgroundSpriteTalking;
		//TODO set PC sprite in pcImage

		[Header("Player State")]
		[SerializeField] private BoolVariable isInDialog;
		[SerializeField] private BoolVariable gamePaused;

		private InputActionsAsset inputActions;

		private DialogManager dialogManager;
		private List<string> dialogOptions = new List<string>();
		private DialogUpdateEventListener dialogUpdateEventListener;
		private StartConversationEventListener startConversationEventListener;
		private FinishedDialogEventListener finishedDialogEventListener;
		private EndConversationEventListener endConversationEventListener;
		private TextMeshProUGUI[] buttonText = new TextMeshProUGUI[3];
		private Image[] buttonSelectImages = new Image[3];

		private int indexOfFocus;
		private int shiftCount;
		private int maxShiftCount;
		private int optionCount;
		private bool isChoosingOptions;

		#endregion

		#region Unity Event Functions

		private void OnEnable() {
			dialogUpdateEvent.RegisterListener(dialogUpdateEventListener);
			finishDialogEvent.RegisterListener(finishedDialogEventListener);
			endConversationEvent.RegisterListener(endConversationEventListener);
			startConversationEvent.RegisterListener(startConversationEventListener);

			inputActions = new InputActionsAsset();
			inputActions.UI.SetCallbacks(this);
			inputActions.UI.Enable();
		}

		private void Awake() {
			dialogUpdateEventListener.controller = this;
			startConversationEventListener.controller = this;
			finishedDialogEventListener.controller = this;
			endConversationEventListener.controller = this;
		}

		private void Start() {
			//Sound
			dialogeSoundManager = GameObject.Find("DialogeSoundManager");
			uiSounds = dialogeSoundManager.GetComponent<Scr_MenuSounds>();

			dialogManager = dialogManagerGameObject.GetComponent<DialogManager>();

			for (int i = 0; i < 3; i++) {
				buttonText[i] = buttons[i].GetComponentInChildren<TextMeshProUGUI>();
				buttonSelectImages[i] = buttons[i].GetComponentInChildren<Image>();
			}
		}

		private void OnDisable() {
			inputActions.UI.Disable();
		}

		#endregion

		#region Input Callbacks

		public void OnNavigate(InputAction.CallbackContext context) {
			bool pressedUp = context.ReadValue<Vector2>().y > 0;
			bool pressedDown = context.ReadValue<Vector2>().y < 0;

			if (!context.performed || !isInDialog.Value || gamePaused.Value) return;
			if (pressedUp) {
				ShiftSelectionUp();
				//GameObject.Find("DialogeSoundManager").GetComponent<Scr_MenuSounds>().PlaySelectSound();
				uiSounds.PlaySelectSound();
			}

			if (pressedDown) {
				ShiftSelectionDown();
				//GameObject.Find("DialogeSoundManager").GetComponent<Scr_MenuSounds>().PlaySelectSound();
				uiSounds.PlaySelectSound();
			}
		}

		public void OnSubmit(InputAction.CallbackContext context) {
			if (!context.performed || !isInDialog.Value || gamePaused.Value) return;
			Confirm();
			//GameObject.Find("DialogeSoundManager").GetComponent<Scr_MenuSounds>().PlayClickSound();
			uiSounds.PlayClickSound();
		}

		public void OnCancel(InputAction.CallbackContext context) {
			if (!context.performed || !isInDialog.Value || gamePaused.Value) return;
			Cancel();
			//GameObject.Find("DialogeSoundManager").GetComponent<Scr_MenuSounds>().PlayCancelSound();
			uiSounds.PlayCancelSound();
		}

		public void OnPause(InputAction.CallbackContext context) { }

		//TODO Interaction with Mouse
		public void OnPointer(InputAction.CallbackContext context) { }

		public void OnClick(InputAction.CallbackContext context) { }

		#endregion

		#region Logic and Visuals Methods

		public void SelectDialog(int index) {
			dialogManager.ChooseDialogFromIndex(index);
			ShowCurrentDialog();
			dialogTextPanel.SetActive(true);
			dialogIconsPanel.SetActive(true);
			dialogOptionPanel.SetActive(false);
		}

		private void ShowCurrentDialog() {
			DialogItem currentDialogItem = dialogManager.CurrentDialogItem();
			dialogText.text = currentDialogItem.text;

			DialogParticipant currentParticipant = dialogManager.GetCurrentParticipant(currentDialogItem.participant);

			if (currentParticipant.isPC) {
//				npcImage.enabled = false;
//				npcName.enabled = false;
//				npcBackgroundImage.sprite = npcBackgroundSpriteDefault;
//				pcBackgroundImage.sprite = pcBackgroundSpriteTalking;
				pcImage.sprite = currentParticipant.portrait;
//				pcImage.enabled = true;
				pcName.text = currentParticipant.characterName;
//				pcName.enabled = true;
				pcIcon.SetActive(true);
				npcIcon.SetActive(false);
			} else {
//				pcImage.enabled = false;
//				pcName.enabled = false;
//				pcBackgroundImage.sprite = pcBackgroundSpriteDefault;
//				npcBackgroundImage.sprite = npcBackgroundSpriteTalking;
				npcImage.sprite = currentParticipant.portrait;
//				npcImage.enabled = true;
				npcName.text = currentParticipant.characterName;
//				npcName.enabled = true;
				pcIcon.SetActive(false);
				npcIcon.SetActive(true);
			}
		}

		private void ShowDialogOptions() {
			dialogOptions = dialogManager.CurrentDialogOptions();
			if (dialogOptions.Count == 0) {
				EndDialog();
			} else {
				isChoosingOptions = true;

				indexOfFocus = 0;
				shiftCount = 0;

				int size = dialogOptions.Count;

				for (int i = 0; i < 3; i++) {
					buttons[i].gameObject.SetActive(true);
				}

				if (size < 3) {
					maxShiftCount = 0;
					for (int i = size; i < 3; i++) {
						buttons[i].gameObject.SetActive(false);
					}

					optionCount = size;
				} else {
					maxShiftCount = size - 3;

					optionCount = 3;
				}

				UpdateDialogOptions();
				UpdateFocus();

				dialogTextPanel.SetActive(false);
				dialogIconsPanel.SetActive(true);
				pcIcon.SetActive(false);
				npcIcon.SetActive(false);
				dialogOptionPanel.SetActive(true);
			}
		}

		private void UpdateDialogOptions() {
			for (int i = 0; i < optionCount; i++) {
				buttonText[i].text = dialogOptions[i + shiftCount];
			}
		}

		private void UpdateFocus() {
			for (int i = 0; i < 3; i++) {
				buttonSelectImages[i].sprite = i == indexOfFocus ? selectedSprite : defaultSprite;
			}
		}

		private void ShiftSelectionUp() {
			if (indexOfFocus == 0) {
				ShiftDialogOptionUp();
			} else {
				indexOfFocus--;
				UpdateFocus();
			}
		}

		private void ShiftSelectionDown() {
			if (indexOfFocus == optionCount - 1) {
				ShiftDialogOptionDown();
			} else {
				indexOfFocus++;
				UpdateFocus();
			}
		}

		private void ShiftDialogOptionUp() {
			if (shiftCount == 0) { } else {
				shiftCount--;
				UpdateDialogOptions();
			}
		}

		private void ShiftDialogOptionDown() {
			if (shiftCount == maxShiftCount) { } else {
				shiftCount++;
				UpdateDialogOptions();
			}
		}

		private bool ProgressDialog() {
			return dialogManager.ProgressDialog();
		}

		private void EndDialog() {
			dialogManager.SayGoodbye();
		}

		private void ShowDialog() {
			dialogPanel.SetActive(true);
		}

		private void HideDialog() {
			dialogPanel.SetActive(false);
		}

		private void Confirm() {
			if (isChoosingOptions) {
				SelectDialog(indexOfFocus + shiftCount);
				isChoosingOptions = false;
			} else {
				if (ProgressDialog()) {
					ShowCurrentDialog();
				}
			}
		}

		private void Cancel() {
			if (isChoosingOptions) {
				EndDialog();
				HideDialog();
				isInDialog.Value = false; //TODO better set this in dialogManager, but temporarily here
			}
		}

		#endregion
	}
}
