﻿using UnityEngine;

namespace Scripts.Dialogs {

	// holds information about a person or object in a dialog
	[CreateAssetMenu(fileName = "New DialogParticipant", menuName = "West Arctica/Dialogs/DialogParticipant")]
	public class DialogParticipant : ScriptableObject {
		public string characterName;

		public bool isPC;

		public Sprite portrait;
	}
}
