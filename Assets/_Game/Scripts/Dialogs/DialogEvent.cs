using UnityAtoms;
using UnityEngine;

namespace Scripts.Dialogs {
	/// <summary>
	/// Base Event used for notifying certain game systems such as UI about dialog progress.
	/// </summary>
	[CreateAssetMenu(fileName = "New DialogEvent", menuName = "West Arctica/Dialogs/DialogEvent", order = 3)]
	public class DialogEvent : GameEvent<DialogSet>{

	}
}
