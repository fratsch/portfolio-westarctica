using System;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Dialogs {
	/// <summary>
	/// Holds information about a DialogGiver. Includes all Dialogs associated with this NPC or Object as well as "goodbyes" for ending conversations.
	/// </summary>
	[Serializable]
	public class DialogGiver {
		[Tooltip("A List with all the Dialogs this DialogGiver can give to the player. When the DialogGiver is activated it sends a DialogSet with all unlocked and uncompleted Dialogs to the DialogManager.")]
		[SerializeField]
		private List<Dialog> dialogsForPlayer = new List<Dialog>();

		// dialogs which are shown upon ending conversation
		[Tooltip("A List with all the goodbye Dialogs.")]
		[SerializeField]
		private List<Dialog> goodbyes = new List<Dialog>();

		[SerializeField] private DialogEvent newDialogEvent;

		// Raises a NewDialogEvent to notify the DialogManager
		public void StartConversation() {
			DialogSet dialogSet = new DialogSet();
			foreach (Dialog dialog in dialogsForPlayer) {
				if (dialog.Unlocked && !dialog.Completed) {
					dialogSet.AddDialog(dialog);
				}
			}

			foreach (Dialog dialog in goodbyes) {
				dialogSet.AddGoodbye(dialog);
			}

			if (dialogSet.DialogOptions().Count != 0) {
				dialogSet.InitConversation(newDialogEvent);
			}
		}
	}
}
