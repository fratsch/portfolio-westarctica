using System;
using System.Collections.Generic;
using Scripts.Questing;
using UnityEngine;

namespace Scripts.Dialogs {
	/// <summary>
	/// Holds information about one dialog. Includes Quests that are unlocked or given as well as furthers dialogs that are unlocked
	/// on completion.
	/// </summary>
	[CreateAssetMenu(fileName = "New Dialog", menuName = "West Arctica/Dialogs/Dialog", order = 1)]
	public class Dialog : ScriptableObject {
		[Header("Dialog Information")]
		[Tooltip("The text that is shown as a selection prompt in the menu.")]
		[SerializeField]
		public string dialogPrompt;

		[Tooltip("List of DialogParticipants")]
		[SerializeField]
		private List<DialogParticipant> dialogParticipants = new List<DialogParticipant>();

		[Header("Dialog Content")]
		[Tooltip("A List with all DialogItems for this Dialog.")]
		[SerializeField]
		private List<DialogItem> dialogItems = new List<DialogItem>();

		[Header("Dialogs")]
		[Tooltip("A List with all the Dialogs that are unlocked after this Dialog is completed.")]
		[SerializeField]
		private List<Dialog> dialogsToUnlock = new List<Dialog>();

		[Header("Questing")]
		[Tooltip("A List with the Quests that the Player is given after this Dialog is completed.")]
		[SerializeField]
		private List<Quest> quests = new List<Quest>();

		[Tooltip("A List with all the Dialogs that are unlocked after this Dialog is completed.")]
		[SerializeField]
		private List<Quest> questsToUnlock = new List<Quest>();

		// Indicates completion of dialog
		public bool Completed { get; private set; }

		[Tooltip("Set if Dialog is unlocked by default.")]
		[SerializeField]
		private bool isUnlocked;

		// Indicates if dialog is unlocked and displayable
		public bool Unlocked { get; set; }

		[Tooltip("Show if the Dialog can only be started once.")]
		public bool isRepeatable;

		private void OnEnable() {
			Unlocked = isUnlocked;
			Completed = false;
		}

		// returns the current DialogItem
		public DialogItem DialogItemAtIndex(int index) {
			return dialogItems[index];
		}

		// returns the number of DialogItems
		public int Length() {
			return dialogItems.Count;
		}

		// completes the Dialog; gives Quests to the player if available; unlocks Quests and Dialogs if available
		public void FinishDialog() {
			Completed = !isRepeatable;
			foreach (Quest quest in quests) {
				quest.InitQuest();
			}

			foreach (Quest quest in questsToUnlock) {
				quest.Unlocked = true;
			}

			foreach (Dialog dialog in dialogsToUnlock) {
				dialog.Unlocked = true;
			}
		}

		// returns the currently talking participant
		public DialogParticipant GetParticipant(int index) {
			return dialogParticipants[index];
		}
	}

	/// <summary>
	/// Holds information about one "line" in a dialog. Includes speaker and text.
	/// </summary>
	[Serializable]
	public struct DialogItem {
		[Tooltip("The index of the participant this DialogItem is associated with.")]
		public int participant;

		[Tooltip("The text that this DialogItem contains.")]
		[TextArea]
		public string text;
	}
}
