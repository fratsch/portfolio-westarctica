using System.Collections.Generic;
using UnityAtoms;
using UnityEngine;

namespace Scripts.Dialogs {
	/// <summary>
	/// Manages dialog progression. Keeps track of current dialog with its progress and raises
	/// corresponding events depending on the changes.
	/// </summary>
	public class DialogManager : MonoBehaviour, IGameEventListener<DialogSet> {
		// the currently active DialogSet
		private DialogSet currentDialogSet;

		// the currently selected Dialog
		private Dialog selectedDialog;

		// the index of the current DialogItem
		private int dialogIndex;

		// the total length of the Dialog
		private int dialogLength;

		// shows if the conversation ends after the current Dialog is finished
		private bool farewell;

		// show if a conversation is happening currently
		private bool talking;

		// shows if a dialog was chosen
		private bool dialogChosen;

		[Tooltip("VoidEvent that is raised when the Dialog changes state.")]
		[SerializeField]
		private VoidEvent dialogUpdateEvent;

		[Tooltip("VoidEvent that is raised when after a new DialogSet was sent to the DialogManager.")]
		[SerializeField]
		private VoidEvent startConversationEvent;

		[Tooltip("VoidEvent that is raised when the Dialog changes state.")]
		[SerializeField]
		private VoidEvent endConversationEvent;

		[Tooltip("DialogEvent that is raised when a new Dialog was initiated.")]
		[SerializeField]
		private DialogEvent newDialogEvent;

		[Tooltip("VoidEvent that is raised when the Dialog is finished.")]
		[SerializeField]
		private VoidEvent finishDialogEvent;

		[SerializeField] private BoolVariable isInDialog;

		// registers itself for the newDialogEvent
		private void OnEnable() {
			newDialogEvent.RegisterListener(this);
		}

		// unregisters itself for the newDialogEvent
		private void OnDisable() {
			newDialogEvent.UnregisterListener(this);
		}

		// returns the current DialogItem
		public DialogItem CurrentDialogItem() {
			return selectedDialog.DialogItemAtIndex(dialogIndex);
		}

		// returns the prompts for the Dialogs in the current DialogSet
		public List<string> CurrentDialogOptions() {
			talking = true;
			return currentDialogSet.DialogOptions();
		}

		// sets a new DialogSet as the current; raises DialogUpdateEvent
		private void AddNewDialogSet(DialogSet newDialogSet) {
			currentDialogSet = newDialogSet;
			startConversationEvent.Raise();
			isInDialog.Value = true;
		}

		// sets a new Dialog as the current one; raises DialogUpdateEvent
		public void ChooseDialogFromIndex(int index) {
			selectedDialog = currentDialogSet.DialogAtIndex(index);
			dialogLength = selectedDialog.Length();
			dialogIndex = 0;
			dialogChosen = true;
			dialogUpdateEvent.Raise();
		}

		// progresses the current Dialog; raises either a DialogUpdateEvent or a FinishDialogEvent
		public bool ProgressDialog() {
			if (!dialogChosen) return false;

			if (dialogIndex < dialogLength - 1) {
				dialogIndex++;
				dialogUpdateEvent.Raise();
				return true;
			} else {
				selectedDialog.FinishDialog();
				dialogChosen = false;
				if (selectedDialog.Completed) {
					currentDialogSet.RemoveDialog(selectedDialog);
				}

				if (farewell) {
					EndConversation();
					return false;
				} else {
					finishDialogEvent.Raise();
					return true;
				}
			}

		}

		// returns the currently speaking participant in this dialog
		public DialogParticipant GetCurrentParticipant(int index) {
			return selectedDialog.GetParticipant(index);
		}

		// sets a farewell Dialog as the current Dialog and marks conversation to be finished; raises DialogUpdateEvent
		public void SayGoodbye() {
			if (!talking || farewell) return;

			selectedDialog = currentDialogSet.Farewell();
			dialogLength = selectedDialog.Length();
			dialogIndex = 0;
			dialogChosen = true;
			farewell = true;
			dialogUpdateEvent.Raise();
		}

		// ends the conversation by resetting everything to default; raises endConversationEvent
		private void EndConversation() {
			currentDialogSet = null;
			selectedDialog = null;
			dialogIndex = 0;
			dialogLength = 0;
			farewell = false;
			talking = false;
			endConversationEvent.Raise();
			isInDialog.Value = false;
		}

		// adds a new DialogSet when a NewDialogEvent is raised
		public void OnEventRaised(DialogSet newDialogSet) {
			AddNewDialogSet(newDialogSet);
		}
	}
}
