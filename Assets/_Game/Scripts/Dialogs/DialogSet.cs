using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace Scripts.Dialogs {
	/// <summary>
	/// Holds all the dialogs and "goodbyes" for an initiated conversation. Dialogs are presented to the player as options in UI
	/// and one "goodbye" is randomly chosen upon ending conversation
	/// </summary>
	[Serializable]
	public class DialogSet {
		[Tooltip("A List of all the Dialogs in this DialogSet")]
		[SerializeField]
		private List<Dialog> dialogs = new List<Dialog>();

		[Tooltip("A List of all the farewell Dialogs in this DialogSet")]
		[SerializeField]
		private List<Dialog> farewellDialog = new List<Dialog>();

		// raises a NewDialogEvent and passes this DialogSet
		public void InitConversation(DialogEvent dialogEvent) {
			dialogEvent.Raise(this);
		}

		// returns a list of all the prompts for the Dialogs
		public List<string> DialogOptions() {
			List<string> dialogOptions = new List<string>();
			foreach (Dialog dialog in dialogs) {
				dialogOptions.Add(dialog.dialogPrompt);
			}

			return dialogOptions;
		}

		// returns the Dialog that corresponds to a given index
		public Dialog DialogAtIndex(int index) {
			return dialogs[index];
		}

		// adds a Dialog to this DialogSet
		public void AddDialog(Dialog dialog) {
			dialogs.Add(dialog);
		}

		// adds a farewell Dialog to this DialogSet
		public void AddGoodbye(Dialog goodbye) {
			farewellDialog.Add(goodbye);
		}

		//removes a Dialog from this DialogSet
		public void RemoveDialog(Dialog dialog) {
			dialogs.Remove(dialog);
		}

		// returns a random farewell Dialog
		public Dialog Farewell() {
			Random random = new Random();
			return farewellDialog[random.Next(farewellDialog.Count)];
		}
	}
}
