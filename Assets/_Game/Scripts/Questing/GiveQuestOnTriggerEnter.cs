﻿// written by Sebastian Wölffel

using Scripts.Questing;
using UnityEngine;

public class GiveQuestOnTriggerEnter : MonoBehaviour {
	[SerializeField] private QuestGiver questGiver;

	private void OnTriggerEnter(Collider other) {
		questGiver.GiveQuestsToPlayer();
	}
}
