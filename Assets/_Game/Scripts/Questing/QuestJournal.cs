using System.Collections.Generic;
using UnityAtoms;
using UnityEngine;

namespace Scripts.Questing {
	/// <summary>
	/// Manages Questing. Includes lists of active and completed Quests. Raises Events on quest progression and changes.
	/// </summary>
	public class QuestJournal : MonoBehaviour {

		// Listener for the NewQuestEvent
		private struct NewQuestEventListener : IGameEventListener<Quest> {
			public QuestJournal journal;
			public void OnEventRaised(Quest quest) {
				journal.NewQuest(quest);
			}
		}

		// Listener for the FinishedQuestEvent
		private struct FinishedQuestEventListener : IGameEventListener<Quest> {
			public QuestJournal journal;
			public void OnEventRaised(Quest quest) {
				journal.FinishedQuest(quest);
			}
		}

		[Tooltip("A List of all Quests which are currently active.")]
		[SerializeField]
		private List<Quest> activeQuests = new List<Quest>();

		[Tooltip("A List of all Quest that have been completed.")]
		[SerializeField]
		private List<Quest> completedQuests = new List<Quest>();

		// Listeners for NewQuestEvents and FinishedQuestEvents
		private NewQuestEventListener newListener;
		private FinishedQuestEventListener finishedListener;

		[Tooltip("Reference to the NewQuestEvent. It is raised when a new Quest was activated.")]
		[SerializeField] private QuestEvent newQuestEvent;

		[Tooltip("Reference to the FinishedQuestEvent. It is raised when a Quest is completed.")]
		[SerializeField] private QuestEvent finishedQuestEvent;

		[Tooltip("Reference to the JournalUpdateEvent. It is raised when the journal contents change.")]
		[SerializeField] private VoidEvent journalUpdateEvent;

		// sets references to the QuestJournal in both listeners
		private void Awake() {
			newListener.journal = this;
			finishedListener.journal = this;
		}

		// registers QuestEventListeners for the events for new and finished Quests
		private void OnEnable() {
			newQuestEvent.RegisterListener(newListener);
			finishedQuestEvent.RegisterListener(finishedListener);
		}

		// unregisters QuestEventListeners for the events for new and finished Quests
		private void OnDisable() {
			newQuestEvent.UnregisterListener(newListener);
			finishedQuestEvent.UnregisterListener(finishedListener);
		}

		// adds a new Quest to the active Quests in the QuestJournal, raises a JournalUpdateEvent
		public void NewQuest(Quest quest) {
			activeQuests.Add(quest);
			journalUpdateEvent.Raise();
		}

		// removes a Quest from the active Quests in the QuestJournal, raises a JournalUpdateEvent
		public void FinishedQuest(Quest quest) {
			activeQuests.Remove(quest);
			completedQuests.Add(quest);
			journalUpdateEvent.Raise();
		}

		// return a List of all active Quests
		public List<Quest> ActiveQuests() {
			List<Quest> questList = new List<Quest>(activeQuests);
			return questList;
		}

		// returns a List of all completed Quests
		public List<Quest> CompletedQuest() {
			List<Quest> questList = new List<Quest>(completedQuests);
			return questList;
		}
	}
}
