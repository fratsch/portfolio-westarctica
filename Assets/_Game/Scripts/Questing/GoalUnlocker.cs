using System;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Questing {
	/// <summary>
	/// Holds a list of Goals to be unlocked upon a certain event.
	/// </summary>
	[CreateAssetMenu(fileName = "New GoalUnlocker", menuName = "West Arctica/Questing/GoalUnlocker", order = 4)]
	public class GoalUnlocker : ScriptableObject {
		[Tooltip("A reference to the Goal this Unlocker unlocks.")]
		[SerializeField]
		private List<Goal> goalsToUnlock;

		// unlocks a corresponding Goal
		public bool UnlockGoal() {
			bool unlocked = false;

			foreach (Goal goal in goalsToUnlock) {
				if (goal.ActivateGoal()) {
					unlocked = true;
				}
			}

			return unlocked;
		}
	}
}
