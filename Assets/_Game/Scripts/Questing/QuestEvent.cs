using UnityAtoms;
using UnityEngine;

namespace Scripts.Questing {
	/// <summary>
	/// Base Event for notifying game system about quest progress
	/// </summary>
	[CreateAssetMenu(fileName = "New QuestEvent", menuName = "West Arctica/Questing/QuestEvent", order = 5)]
	public class QuestEvent : GameEvent<Quest> {

	}
}
