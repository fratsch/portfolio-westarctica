using System.Collections.Generic;
using UI;
using UnityAtoms;
using UnityEngine;

namespace Scripts.Questing {
	/// <summary>
	/// Holds information about one Goal. Includes name and description for displaying as well as progress
	/// and state of the Goal. A Goal represents a smaller part of a Quest
	/// </summary>
	[CreateAssetMenu(fileName = "New Goal", menuName = "West Arctica/Questing/Goal", order = 2)]
	public class Goal : ScriptableObject {
		[Header("Goal Information")]
		[Tooltip("The title of this Goal.")]
		public string title;

		[Tooltip("The description of this Goal.")]
		[TextArea]
		public string description;

		[Header("Goal State")]
		[SerializeField]
		private int requiredProgress;

		// the current amount of progress towards this Goal
		private int CurrentProgress { get; set; }

		// shows if the Goal is unlocked (Quest is active)
		private bool IsUnlocked { get; set; }

		[Header("Goal Flags")]
		[Tooltip("Shows if this Goal should be active when the Quest starts. All Goals flagged are set to active when the Quest is started.")]
		[SerializeField]
		private bool isStartingGoal;

		[Tooltip("Shows if this Goal is optional for the Quest. Optional Goals are ignored if the Quest is checked for completion.")]
		public bool optional;

		[Tooltip("Shows if this Goal is currently active. Only active Goals get progress when a GoalTrigger is activated.")]
		public bool active;

		[Tooltip("Shows if this Goal has been completed. All non-optional Goals of a Quest have to be completed for the Quest to be completed.")]
		public bool completed;

		[Header("Quest Management")]
		[Tooltip("A List of Goals that get unlocked if the current Goal is completed.")]
		[SerializeField]
		private List<Goal> followUpGoals = new List<Goal>();

		[SerializeField] private VoidEvent goalUpdateEvent;

		[SerializeField] private HudNotificationEvent hudNotificationEvent;

		[SerializeField] private Sprite notificationSprite;

		// initializes the Goals when Quest is started
		public void InitGoal() {
			CurrentProgress = 0;
			active = false;
			IsUnlocked = true;
			completed = false;
			if (isStartingGoal) {
				ActivateGoal();
			}
		}

		// if active progresses the Goal and checks for completion
		public void ProgressGoal() {
			if (!active || completed) return;

			CurrentProgress++;
			Check();
			goalUpdateEvent.Raise();
			HudNotification notification = new HudNotification {
				text = "Fortschritt: " + title + " " + CurrentProgress + "/" + requiredProgress,
				icon = notificationSprite
			};
			hudNotificationEvent.Raise(notification);
		}

		// flags Goal as active
		public bool ActivateGoal() {
			if (completed || !IsUnlocked || active) return false;

			HudNotification notification = new HudNotification {
				text = "Neue Aufgabe: " + title + " " + CurrentProgress + "/" + requiredProgress,
				icon = notificationSprite
			};
			hudNotificationEvent.Raise(notification);

			active = true;

			return true;
		}

		// checks CurrentProgress against requiredProgress to see if Goal is reached
		private void Check() {
			if (CurrentProgress < requiredProgress) return;
			Complete();
		}

		// completes Goal and unlocks all following Goals
		private void Complete() {
			completed = true;
			goalUpdateEvent.Raise();
			foreach (Goal goal in followUpGoals) {
				goal.ActivateGoal();
			}
		}
	}
}
