using UnityEngine;
using UnityEngine.Serialization;

namespace Scripts.Questing {
	/// <summary>
	/// Represents the target of a Goal. Holds information about the associated Goal and can add progress to it.
	/// </summary>
	[CreateAssetMenu(fileName = "New GoalTarget", menuName = "West Arctica/Questing/GoalTarget", order = 3)]
	public class GoalTarget : ScriptableObject {
		[Tooltip("A reference to the Goal this Target progresses.")]
		[SerializeField]
		private Goal targetedGoal;

		// progresses the corresponding Goal
		public void TargetProgress() {
			targetedGoal.ProgressGoal();
		}
	}
}
