using System;
using System.Collections.Generic;
using System.Linq;
using Scripts.Dialogs;
using UI;
using UnityAtoms;
using UnityEngine;
using Void = UnityAtoms.Void;

namespace Scripts.Questing {
	/// <summary>
	/// Holds information about a Quest. Includes of list of Goals which have to be completed to finish the Quest, as well as
	/// a list of Quests and Dialogs to be unlocked upon Quest completion. Raises Events to notify other system about progress.
	/// </summary>
	[CreateAssetMenu(fileName = "New Quest", menuName = "West Arctica/Questing/Quest", order = 1)]
	public class Quest : ScriptableObject, IGameEventListener<Void> {
		[Header("Quest-Information")]
		[Tooltip("The title of this Quest.")]
		public string title;

		[Tooltip("The description of this Quest.")]
		[TextArea]
		public string description;

		[Header("Quest Contents")]
		[Tooltip("A List of all the Goals associated with this Quests. The Quest is complete when all non optional Goals are completed.")]
		[SerializeField]
		private List<Goal> goals = new List<Goal>();

		[Tooltip("A List of all the Quests which are follow ups to this one. These Quests get unlocked once this one has been completed.")]
		[SerializeField]
		private List<Quest> followUpQuests = new List<Quest>();

		[Tooltip("A List with all the Dialogs that are unlocked when this Dialog is completed.")]
		[SerializeField]
		private List<Dialog> unlockDialogs = new List<Dialog>();

		[Header("Quest Management")]
		[Tooltip("Event that gets raised when this Quest is activated.")]
		[SerializeField]
		private QuestEvent newQuestEvent;

		[Tooltip("Event that gets raised when this Quest is finished.")]
		[SerializeField]
		private QuestEvent finishedQuestEvent;

		public bool Completed { get; set; }

		[Tooltip("Shows if this Quest is unlocked and can be registered at the QuestManager.")]
		[SerializeField]
		private bool isUnlocked;

		[SerializeField] private Sprite notificationSprite;

		public bool Unlocked { get; set; }

		public bool active; // shows if the Quest is currently active and registered at the QuestManager

		[SerializeField] private VoidEvent goalUpdateEvent;

		[SerializeField] private HudNotificationEvent hudNotificationEvent;

		private void OnEnable() {
			goalUpdateEvent.RegisterListener(this);
			Unlocked = isUnlocked;
			Completed = false;
			active = false;
		}

		private void OnDisable() {
			goalUpdateEvent.UnregisterListener(this);
		}

		// initializes the Quest and all Goals when its started
		public void InitQuest() {
			active = true;
			foreach (Goal goal in goals) {
				goal.InitGoal();
			}

			newQuestEvent.Raise(this);
			HudNotification notification = new HudNotification() {
				text = "Neue Quest erhalten!",
				icon = notificationSprite
			};
			hudNotificationEvent.Raise(notification);
		}

		// checks all Goals to see if they are either completed or optional
		private void CheckGoals() {
			if (goals.All(g => g.completed || g.optional) && !Completed) {
				CompleteQuest();
			}
		}

		// completes the Quest, unlocks following Quests and Dialogs; fires finishedQuestEvent
		private void CompleteQuest() {
			foreach (Quest quest in followUpQuests) {
				quest.Unlocked = true;
			}

			foreach (Dialog dialog in unlockDialogs) {
				dialog.Unlocked = true;
			}

			Completed = true;
			active = false;
			finishedQuestEvent.Raise(this);
			HudNotification notification = new HudNotification {
				text = "Quest abgeschlossen!",
				icon = notificationSprite
			};
			hudNotificationEvent.Raise(notification);
		}

		// return a List with all Goal currently active for this quest
		public List<Goal> ActiveGoals() {
			List<Goal> result = new List<Goal>();

			foreach (Goal goal in goals) {
				if (goal.active) {
					result.Add(goal);
				}
			}

			return result;
		}

		public void OnEventRaised(Void item) {
			CheckGoals();
		}
	}
}
