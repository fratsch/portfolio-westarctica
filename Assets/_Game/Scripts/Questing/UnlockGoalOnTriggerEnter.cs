// written by Sebastian Wölffel

using System;
using System.Collections;
using System.Collections.Generic;
using Scripts.Questing;
using UnityEngine;

public class UnlockGoalOnTriggerEnter : MonoBehaviour {
	[SerializeField] private GoalUnlocker goalUnlocker;

	//Sounds
	[SerializeField]
	private GameObject questSoundController;
	private QuestSounds questSounds;

	private void Start()
	{
		//Sounds
		questSoundController = GameObject.FindGameObjectWithTag("QuestSoundController");
		if (questSounds != null)
		{
			questSounds = questSoundController.GetComponent<QuestSounds>();
		}
	}

	//Sounds
	//questSounds.PlayQuestSound(2);

	private void OnTriggerEnter(Collider other) {

		//goalUnlocker.UnlockGoal();

		if (questSoundController != null && goalUnlocker.UnlockGoal())
		{
			questSounds.PlayQuestSound(0);
		}
	}
}
