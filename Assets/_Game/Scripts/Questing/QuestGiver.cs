using System;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Questing {
	/// <summary>
	/// Holds information about a quest giver. Includes list of Quests. Only unlocked Quests are given to the player.
	/// </summary>
	[Serializable]
	public class QuestGiver {
		[Tooltip("A List with all the Quests this QuestGiver can give to the player. When the QuestGiver is activated he registers all Quests that are unlocked and not yet active or completed.")]
		[SerializeField]
		private List<Quest> questsForPlayer = new List<Quest>();

		// initiates all unlocked and inactive Quests
		public void GiveQuestsToPlayer() {
			foreach (Quest quest in questsForPlayer) {
				if (quest.Unlocked && !quest.active) {
					quest.InitQuest();
				}
			}
		}
	}
}
