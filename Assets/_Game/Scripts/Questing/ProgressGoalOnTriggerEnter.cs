﻿// written by Sebastian Wölffel

using System;
using System.Collections;
using System.Collections.Generic;
using Scripts.Questing;
using UnityEngine;

public class ProgressGoalOnTriggerEnter : MonoBehaviour {
	[SerializeField] private GoalTarget goalTarget;

	private void OnTriggerEnter(Collider other) {
		goalTarget.TargetProgress();
	}
}
