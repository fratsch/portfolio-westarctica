using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Questing {
	/// <summary>
	/// Holds several quests that should be unlocked upon a certain trigger (could be completion of a previous dialog or quest)
	/// </summary>
	[CreateAssetMenu(fileName = "New QuestUnlocker", menuName = "West Arctica/Questing/QuestUnlocker", order = 5)]
	public class QuestUnlocker : ScriptableObject {
		[Tooltip("The Quests that should be unlocked if this QuestUnlocker is activated")]
		[SerializeField] private List<Quest> questsToUnlock = new List<Quest>();

		// unlocks all Quests
		public void UnlockQuests() {
			foreach (Quest quest in questsToUnlock) {
				quest.Unlocked = true;
			}
		}
	}
}
