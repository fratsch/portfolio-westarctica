﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowController : MonoBehaviour {
	private Animator anim;
	private static readonly int TriggerPullString = Animator.StringToHash("TriggerPullString");
	private static readonly int TriggerShot = Animator.StringToHash("TriggerShot");

	private void Start() {
		anim = GetComponent<Animator>();
	}

	public void PullStringAnim(bool val) {
		if (!val) return;
		anim.SetTrigger(TriggerPullString);
	}

	public void ShootAnimation(bool val) {
		if (val) return;
		anim.SetTrigger(TriggerShot);
	}
}
