using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class ProjectileOnHit : MonoBehaviour {
	public int damage;

	private void OnCollisionEnter(Collision other) {
		ExecuteEvents.Execute<IHealth>(other.gameObject, null, (x, y) => x.Damage(damage));
		print("Collision");
		Destroy(gameObject);
	}
}
