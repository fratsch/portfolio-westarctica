﻿using System;
using UnityEngine;

public class ProjectileMovement : MonoBehaviour {
	private Transform _transform;
	private Rigidbody _rigidbody;


	private void Start() {
		_transform = transform;
		_rigidbody = _transform.GetComponent<Rigidbody>();
	}

	private void Update() {
		if (_rigidbody.velocity.magnitude > 0.1f) {
			_transform.forward = _rigidbody.velocity;
		}
	}
}
