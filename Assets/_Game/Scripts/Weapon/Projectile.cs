using UnityEngine;

//this can hopefully inherit from Item class in the future, which should be scriptable object

[CreateAssetMenu(menuName = "West Arctica/Inventory/Weapon/Projectile")]
public class Projectile : ScriptableObject {
	public GameObject actualGameObject;
	public int damage;
	public float flySpeed;
	public float aliveTime;
}
