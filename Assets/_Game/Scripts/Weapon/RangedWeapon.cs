using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

//this can hopefully inherit from Item class in the future, which should be scriptable object

[CreateAssetMenu(menuName = "West Arctica/Inventory/Weapon/RangedWeapon")]
public class RangedWeapon : ScriptableObject {
	public int baseDamage;
	public float attackRange;
	public Projectile projectile;
	public int currentAmmunition;
	public int maximumAmmunition;

	public LayerMask layersToIgnore;

	public float minProjectileForce = 1.0f;
	public float maxProjectileForce = 2.0f;
	public float drawTime = 2.0f;

	private float projectileForce = 0.0f;

	private GameObject player;
	private Camera mainCam;

	private void OnEnable() {
		//currentAmmunition = maximumAmmunition is just for testing convenience. Should be set on game start from save, TODO
		currentAmmunition = maximumAmmunition;
	}

	public void Draw() {
		projectileForce = minProjectileForce;
		DOTween.To(() => projectileForce, x => projectileForce = x, maxProjectileForce, drawTime).SetEase(Ease.OutCubic);
	}

	public void Shoot(Transform from, Transform to) {
		if (currentAmmunition == 0) return;
		currentAmmunition--;

		DOTween.KillAll();

		GameObject shotProjectile = Instantiate(projectile.actualGameObject, from.position, Quaternion.LookRotation(to.forward));

		if (shotProjectile.GetComponent<ProjectileOnHit>() == null) {
			shotProjectile.AddComponent<ProjectileOnHit>();
		}

		shotProjectile.GetComponent<ProjectileOnHit>().damage = baseDamage + projectile.damage;

		if (shotProjectile.GetComponent<Rigidbody>() == null) {
			shotProjectile.AddComponent<Rigidbody>();
		}

		Vector3 dir;
		if (Physics.Raycast(to.position, to.forward, out RaycastHit hit, attackRange, layersToIgnore, QueryTriggerInteraction.Ignore)) {
			dir = (hit.point - from.position).normalized;
		} else {
			dir = to.forward.normalized;
		}

		Rigidbody shotProjectileBody = shotProjectile.GetComponent<Rigidbody>();
		shotProjectileBody.AddForce(projectile.flySpeed * projectileForce * dir, ForceMode.VelocityChange);
		Destroy(shotProjectile, projectile.aliveTime);
	}

	public void AddAmmo(int ammo) {
		if (currentAmmunition + ammo <= maximumAmmunition) {
			currentAmmunition += ammo;
		} else {
			currentAmmunition = maximumAmmunition;
		}
	}

	public void SetProjectile(Projectile p) {
		projectile = p;
	}

	//maybe add future methods like upgrading, destroying weapon, etc.
}
