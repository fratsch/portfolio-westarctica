﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class Flickering : MonoBehaviour {
	[SerializeField] private float intensityFlicker;
	[SerializeField] private float positionFlicker;
	[SerializeField] private float flickerRate;

	private Light lightSource;

	private Vector3 basePosition;
	private Vector3 startingPosition;
	private Vector3 targetPosition;
	private float baseIntensity;
	private float targetIntensity;

	private bool flickering;

	private float flickerTime;

	public void Start() {
		lightSource = GetComponent<Light>();

		basePosition = transform.position;
		baseIntensity = lightSource.intensity;

		startingPosition = basePosition;

		float x = Random.Range(-positionFlicker, positionFlicker);
		float y = Random.Range(-positionFlicker, positionFlicker);
		float z = Random.Range(-positionFlicker, positionFlicker);
		targetPosition = startingPosition + new Vector3(x, y, z);

		targetIntensity = baseIntensity + Random.Range(-intensityFlicker, intensityFlicker);
	}

	private void Update() {
		if (flickerTime >= flickerRate) {
			float x = Random.Range(-positionFlicker, positionFlicker);
			float y = Random.Range(-positionFlicker, positionFlicker);
			float z = Random.Range(-positionFlicker, positionFlicker);
			targetPosition = basePosition + new Vector3(x, y, z);

			startingPosition = transform.position;

			targetIntensity = baseIntensity + Random.Range(-intensityFlicker, intensityFlicker);

			flickerTime = 0.0f;
		}

		transform.position = Vector3.Lerp(startingPosition, targetPosition, flickerTime / flickerRate);
		lightSource.intensity = Mathf.Lerp(baseIntensity, targetIntensity, flickerTime / flickerRate);

		flickerTime += Time.deltaTime;
	}
}
