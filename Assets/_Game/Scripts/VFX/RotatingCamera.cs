﻿using UnityEngine;

public class RotatingCamera : MonoBehaviour {
	[SerializeField] private GameObject target;
	[SerializeField] private float rotatingSpeed;

	private void Update() {
		transform.RotateAround(target.transform.position , Vector3.up, Time.deltaTime * rotatingSpeed);
		transform.LookAt(target.transform);
	}
}
