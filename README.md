# Portfolio - WestArctica

This team project was part of a module in the "Computer Science - Digital Media and Games" course at the Hochschule Trier.

Our team consisted of five programmers and four designers.

My tasks included the design and implementation of a dialog and quest system. Both quests and dialogs were required to be modular and
(reasonably) easy to change in the editor for designers. The same holds true for quest progression as well as quest and dialog unlocking.
Every part was designed with modularity and expandability in mind. For example quest and dialog givers as well as targets and unlockers
are not limited to AI or NPC, but can be expanded to other objects (i.e. sign posts, letters, etc) as well and can be triggered by different
kind of scripts (i.e. interaction, death, trigger colliders).

Smaller tasks include projectile (arrow) behaviour as well as a campfire effect.

Only files corresponding to these tasks are included since this is part of a university exam.

Files which are needed for context but have not been written by me are marked by a comment in line one.